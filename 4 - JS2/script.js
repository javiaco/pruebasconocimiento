"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

/* Asignamos la función a la constante "sumaPuntuaciones", recibe como parámetro un objeto array (arrPuntuaciones).
   El método map() crea un nuevo array con los resultados de la llamada a la función definida, aplica a cada uno de sus elementos.
   Creamos una variable "nuevoArr" y le asignamos un objeto de clave - valor con los valores vacíos.
   Asignamos los valores de las claves de itemArrPuntuaciones a cada clave de nuevoArr.
   En el caso de los puntosTotales la asignacion será la resultante de la suma de los valores del array puntos aplicando el método reduce.
   La función map devuelve por cada paso de item un objeto con los nuevos valores asignados a las claves.
*/

const sumaPuntuaciones = (arrPuntuaciones) =>
  arrPuntuaciones.map(function (itemArrPuntuaciones) {
    let nuevoArr = {
      equipo: "",
      puntosTotales: "",
    };
    nuevoArr.equipo = itemArrPuntuaciones.equipo;
    nuevoArr.puntosTotales = itemArrPuntuaciones.puntos.reduce(function (
      acc,
      val
    ) {
      return acc + val;
    },
    0);
    return nuevoArr;
  });

console.log(sumaPuntuaciones(puntuaciones));
