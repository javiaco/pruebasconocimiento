function adivinarNumero() {
  let password = Math.round(Math.random() * 100); //generamos numero aleatorio de 0 a 100
  console.log(password);
  let numero = prompt("Intenta adivinar un número de 0 a 100");

  for (let i = 1; i < 5; i++) {
    if (parseInt(numero) === password) {
      alert("Enhorabuena!!! Has ganado");
      return;
    } else {
      if (parseInt(numero) > password) {
        let intento = i + 1;
        numero = prompt(
          "El numero introducido es mayor que el buscado, intento " + intento
        );
      } else {
        let intento = i + 1;
        numero = prompt(
          "El numero introducido es menor que el buscado, intento " + intento
        );
      }
    }
  }
  if (parseInt(numero) === password) {
    alert("Enhorabuena!!! Has ganado");
    return;
  }
  alert("Has perdido, el número era: " + password);
}

adivinarNumero();
